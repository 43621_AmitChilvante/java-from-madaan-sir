/**
 * 
 */
package assignment;

/**
 * @author Nipun Madaan
 *
 */
import java.util.Scanner;
public class Input {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		Integer a = sc.nextInt();  //it is used to take int from user
		System.out.println(a);
		Float b = sc.nextFloat(); //it is used to take float from user
		System.out.println(b);
		Double c = sc.nextDouble(); //it is used to take double from user
		System.out.println(c);
		Long d = sc.nextLong(); //it is used to take long from user
		System.out.println(d);
		Byte e = sc.nextByte(); //it is used to take byte from user
		System.out.println(e);
		String s = sc.next(); //it is used to take single word from user
		System.out.println(s);
		String s1 = sc.nextLine(); //it is used to take whole line from user
		System.out.println(s1);
		Character c1 = sc.next().charAt(0);
		System.out.println(c1);  //used to take a single character
		sc.close();
	}
}

package assignment;

public class Nipun {
	private int x;
	private int y;
	
	Nipun(){
		x=10;
		y=20;
	}
	
	Nipun(int x,int y){
		this.x=x;
		this.y=y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public void setX(int x) {
		this.x=x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y=y;
	}
	public static void main(String[] args) {
		Nipun n1 = new Nipun();
		Nipun n2 = new Nipun(40,50);
		System.out.println(n1.getX());
		System.out.println(n1.getY());
		System.out.println(n2.getX());
		System.out.println(n2.getY());
	}
}

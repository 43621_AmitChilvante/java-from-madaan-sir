package assignment;
public class Amit {

	private int i=5;   //instance variable
	private static int j=10;   //static variable
	
	int fun() {
		int x=10;
		i++;
		System.out.println(x);
		return x;
	}
	
	public static void main(String[] args) {
		Nipun n = new Nipun();
		System.out.println(n.getX());   //10
		n.setX(50);
		System.out.println(n.getX());   //50
		Nipun n2 = new Nipun();
		System.out.println(n2.getX());
		System.out.println("Hello World!!!");
		System.out.println("Hello World!!!");
		Amit a = new Amit();
		a.i++;  //Amit.j++;
		System.out.println(a.i);
		Amit b = new Amit();
		b.i++;  //Amit.j++;
		System.out.println(b.i);
		int x = a.fun();
		b.fun();
		System.out.println(a.i);
		System.out.println(b.i);
		/*System.out.println(a.j);  //Amit.j //a.j
		System.out.println(a.i);
		a.i++;
		int x = a.i+20;
		System.out.println(x + " " + a.i);
		System.out.println(b.i);*/
	}
	
}

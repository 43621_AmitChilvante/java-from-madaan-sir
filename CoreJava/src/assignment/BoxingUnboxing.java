package assignment;

public class BoxingUnboxing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i = 5;
		String s = String.valueOf(i);  //boxing
		System.out.println(s);
		String x = "10";
		int y = Integer.parseInt(x);  //unboxing
		System.out.println(y);
	}

}

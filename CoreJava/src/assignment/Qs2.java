package assignment;

import java.util.Scanner;

public class Qs2 {

	public static void main(String[] args) {
		System.out.println("Enter a number");
		Scanner sc = new Scanner(System.in);
		Integer a = sc.nextInt();
		System.out.println("Binary : "+Integer.toBinaryString(a));
		System.out.println("Octal : "+Integer.toOctalString(a));
		System.out.println("Hexadecimal : "+Integer.toHexString(a));
		sc.close();
	}
}